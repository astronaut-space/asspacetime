# ASSpaceTime

[![CI Status](https://img.shields.io/travis/astronaut/ASSpaceTime.svg?style=flat)](https://travis-ci.org/astronaut/ASSpaceTime)
[![Version](https://img.shields.io/cocoapods/v/ASSpaceTime.svg?style=flat)](https://cocoapods.org/pods/ASSpaceTime)
[![License](https://img.shields.io/cocoapods/l/ASSpaceTime.svg?style=flat)](https://cocoapods.org/pods/ASSpaceTime)
[![Platform](https://img.shields.io/cocoapods/p/ASSpaceTime.svg?style=flat)](https://cocoapods.org/pods/ASSpaceTime)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ASSpaceTime is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ASSpaceTime'
```

## Author

astronaut, z13976100@163.com

## License

ASSpaceTime is available under the MIT license. See the LICENSE file for more info.
